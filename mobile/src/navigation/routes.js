import React from 'react';
import { createAppContainer,createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { MaterialIcons } from '@expo/vector-icons';

import PostView from '../screens/PostsScreens/PostView';
import PostEdit from '../screens/PostsScreens/PostEdit';
import PostAdd from '../screens/PostsScreens/PostAdd';
import Dashboard from '../screens/Dashboard';
import Profile from '../screens/Profile';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';

export default (signedIn = false) => createAppContainer(
      createSwitchNavigator({
          Sign:createSwitchNavigator({
              SignIn,
              SignUp,
          }),
          App:createBottomTabNavigator({
            New:{
                screen:createStackNavigator({
                    Dashboard,
                    PostAdd,
                    PostView,
                    PostEdit,
                },{
                    defaultNavigationOptions:{
                        headerShown:true,
                        headerTransparent:true,
                        headerTintColor: 'red',
                        headerLeftContainerStyle: {
                            marginLeft: 20,
                          },
                    }
                }),
                navigationOptions:{

                tabBarIcon: (
                  <MaterialIcons
                    name="home"
                    size={20}
                    color="#C14242"
                  />
                ),
                }
            },
            Profile

          },{
              tabBarOptions:{
                  keyboardHidesTabBar: true,
                  activeTintColor: '#C14242',
                  inactiveTintColor:'rgba(193,66,66,0.6)',
                  style:{
                      backgroundColor:'#0a0a0a'
                  }
              }
          }),
      },{
          initialRouteName: signedIn ? 'App' : 'Sign'
      }),
  );

