import React,{useState, useEffect} from 'react';
import {TouchableOpacity } from 'react-native';
import {MaterialIcons } from '@expo/vector-icons';
import api from '../../services/api';

import {
     Container,
     ContainerTop,
     ContainerFooter,
     Left,
     Right,
     Avatar,
     Info, Name,
     Post,
     Votes } from './styles';


export default function PostDetail({data,navigation}){
    const [selected, setSelected] = useState(0);
    const[postId,setPostId] = useState();
    const[upvotes, setUpvotes] = useState();


    useEffect(()=>{
        setPostId(data.id)
        setUpvotes(data.upvotes)
        /* async function loadVotes(){
            const response = await api.get('posts');
            setPosts(response.data);
        }
        loadVotes(); */
    });
    function handlePress(){
        setSelected(1);
        return selected;
    }
    async function handleSubmit() {
      await api.post('voting',{
          postId,
      });
        setPostId();



    }
    return(
        <Container>
            <ContainerTop>
                <Left>
                    <Avatar source={{
                uri: data.user.avatar
                ? data.user.avatar.url
                : `https://api.adorable.io/avatar/50/${data.user.name}.png`,
            }}/>
                    <Info>
                        <Name> {data.user.name}</Name>
                        <Post>{data.content}</Post>
                    </Info>
                </Left>
                <Right>
                {/* <TouchableOpacity onPress={()=>{handlePress()}}>
                    <MaterialIcons
                    name="close"
                    size={25}
                    color={'#a0a0a0'}
                    />
                </TouchableOpacity> */}


                </Right>
            </ContainerTop>
            <ContainerFooter>
                <Votes>{upvotes}</Votes>
                <TouchableOpacity onPress={()=>{handleSubmit()}}>
                        <MaterialIcons
                        name="done"
                        size={25}
                        color={'#a0a0a0'}/>
                </TouchableOpacity>
            </ContainerFooter>
        </Container>
    )
}
