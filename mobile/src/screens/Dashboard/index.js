import React,{ useEffect, useState } from 'react';
import{TouchableOpacity} from 'react-native';
import api from '../../services/api';
import { MaterialIcons } from '@expo/vector-icons';
import { Container, Title, List } from './styles';
import PostDetail from '../../components/PostDetail';
import { FloatingAction } from "react-native-floating-action";

const actions = [
    {
        text: "Add",
        icon: require("../../../assets/addpost.png"),
        name: "bt_accessibility",
        position: 1,
        color: 'red'

    }]


export default function Dashboard(props){
    const [ posts, setPosts ] = useState([]);
    useEffect(()=>{
        async function loadPosts(){
            const response = await api.get('posts');
            setPosts(response.data);
        }
        loadPosts();
    });
    return(
        <Container>


            <List
          data={posts}
          keyExtractor={post => String(post.id)}
          renderItem={({ item:post }) => (
                  <PostDetail data={post} navigation={props.navigation}/>

          )}
        />
        <FloatingAction

            color="red"
            margin={1}
            actions={actions}
            onPressItem={()=>props.navigation.navigate('PostAdd')}
        />
        </Container>
    )
}




Dashboard.navigationOptions = {

    tabBarLabel: 'Feed',
    tabBarIcon: ({tintColor}) => <MaterialIcons name="home" size={20} color={tintColor}/>
};
