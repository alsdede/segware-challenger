import React from 'react';
import { View,Text,TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'

// import { Container } from './styles';

export default function PostView(props) {
    const post = props.navigation.getParam('post');
  return (
    <View style={{flex:1}}>
    <Text style={{alignItems:'center', justifyContent:'center'}}>{post.id}</Text>
    </View>
  );
}

PostView.navigationOptions = ( props ) => ({
    title: 'View',
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Dashboard');
        }}
      >
        <MaterialIcons name="chevron-left" size={20} color="#c14242" />
      </TouchableOpacity>
    ),
  });
