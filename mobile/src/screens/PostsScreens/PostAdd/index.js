import React,{useState} from 'react';
import { View,Text, TouchableOpacity } from 'react-native';
import api from '../../../services/api';
import {MaterialIcons} from '@expo/vector-icons';
import {Container,Form,ContainerInput, FormInput, SubmitButton} from './styles';

// import { Container } from './styles';

export default function PostAdd(props) {
    const [content,setContent] = useState('')

    async function handleSubmit() {
        await api.post('posts', {
            content,
          });
          props.navigation.navigate('Dashboard');
    }
  return (
    <Container>
        <Form>
            <ContainerInput>
            <FormInput
            multiline
            numberOfLines={1}
            placeholder="Escreva seu post"
            onChangeText={setContent}
            />
            </ContainerInput>
            <SubmitButton onPress={handleSubmit}>Adicionar Post</SubmitButton>
        </Form>
    </Container>
  );
}


PostAdd.navigationOptions = (props) => ({
    title: 'Adicionar Post',
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('Dashboard');
        }}
      >
        <MaterialIcons name="chevron-left" size={20} color="#c14242" />
      </TouchableOpacity>
    ),
  });
