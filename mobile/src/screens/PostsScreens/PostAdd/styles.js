import styled from 'styled-components/native';
import { Platform } from 'react-native';
import Input from '../../../components/Input';
import Button from '../../../components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
    enabled: Platform.OS === 'ios',
    behavior: 'padding',
  })`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0 20px;


  `;

export const Separator = styled.View`
  height:1px;
  background: rgba(255, 255, 255, 0.2);
  margin: 20px 0 30px;
`;

export const Title = styled.Text`
  font-size: 20px;
  color: #f64c75;
  font-weight: bold;
  align-self: center;
  margin-top: 30px;

`;

export const Form = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
      padding: 30,

     },

})`
  align-self: stretch;

`;

export const ContainerInput = styled.View.attrs({
    style:{
        shadowColor: "#000",
      shadowOffset: {
          width: 1,
          height: 2,
      },
      shadowOpacity: 2.25,
      shadowRadius: 3.84,

      elevation: 5,
    }
})`
    margin-top:80px;
  flex-direction:column;
  height:300px;
  border: 0.5px solid;

`;
export const FormInput = styled.TextInput`




    border-radius:0px;



`;

export const SubmitButton = styled(Button)`
  margin-top: 30px;
`;

export const LogoutButton = styled(Button)`
  margin-top: 10px;
  background: #C14242;
`;
