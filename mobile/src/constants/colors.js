export default {
    primaryColor: '#A0A0',
    accentColor: '#708090',
    textColor: '#A0A0A0',
    backgroundColor: '#f0f0f0',
};
