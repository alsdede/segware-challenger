module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.createTable('votings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      voting: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      user_id:{
        type: Sequelize.INTEGER,
        references:{ model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true,

      },
      post_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'posts',
          key: 'id',
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }),
    down: queryInterface => queryInterface.dropTable('votings')
  };
