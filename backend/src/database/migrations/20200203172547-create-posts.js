
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('posts', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    date: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    content: {
      type: Sequelize.STRING,
      allowNull: false,

    },
    user_id:{
        type: Sequelize.INTEGER,
        references:{ model: 'users', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true,
    },
    upvotes:{
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,

    },
    downvotes: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    created_at: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  }),

  down: (queryInterface) => queryInterface.dropTable('posts'),
};
