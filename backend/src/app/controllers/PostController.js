import Post from '../models/Post';
import User from '../models/User';
import Voting from '../models/Voting';
import * as Yup from 'yup';


class PostController {
    async index(req,res){
        const posts = await Post.findAll({
            order:[['created_at','DESC']],
            include:[{
                model:User,
                as: 'user',
            }]
        })

        return res.json(posts);
    }

    async store(req,res){
        const schema = Yup.object().shape({
            content: Yup.string().required(),

        });

        if(!(await schema.isValid(req.body))){
            return res.json(400).json({error: 'Validation fails'});
        }

        const { content } = req.body;

        /* Check if user_id is a user */
         const isUser = await User.findOne({
            where:{ id: req.userId},

        })

        if(!isUser){
            return res
            .status(401)
            .json({error: 'You can only create post with users, Please login'});
        }
        const post = await Post.create({
            user_id: req.userId,
            date: new Date(),
            content,
        });
        return res.json(post);
    }

}

export default new PostController();
