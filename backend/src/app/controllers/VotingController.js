

import Post from '../models/Post';
import User from '../models/User';
import Voting from '../models/Voting';

import * as Yup from 'yup';


class VotingController {

    async store(req,res){
        /* const schema = Yup.object().shape({
            post_id: Yup.number().required(),
        }); */

        if(!(await schema.isValid(req.body))){
            return res.json(400).json({error: 'Validation fails'});
        }

        const { post_id } = req.body;
        console.log(post_id)
        /* Check if userId is a user */
         const isUser = await User.findOne({
            where:{ id: req.userId},
        });

        if(!isUser){
            return res
            .status(401)
            .json({error: 'You can only voting post with users, Please login'});
        }
        /* const isPost = await Post.findByPk({
            where:{ id: post_id }
        });
        if(!isPost){
            return res
            .status(401)
            .json({error: 'Post not found'});
        }; */

        const isVoting = await Voting.findOne({
            attributes:['value'],
            where:{
                post_id,
                user_id: req.userId,
            },

        });
        if(!isVoting){
         const voting = await Voting.create({
                value: 1,
                user_id: req.userId,
                post_id,
            })
            Post.findOne({
                where:{id: post_id}
            }).then((post) =>{ post.increment('upvotes').then(()=> res.status(201).json({
                message: 'Upvote sucess',
                post
            }))})

            return res.json(post);
        }else if (isVoting === 1){
          const voting =  Voting.destroy({
                where:{
                    user_id: req.userId,
                    post_id,
                }
            }).then(()=>{
                Post.findOne({where:{id: post_id}}).then((post) => {
                    post.update({ upvotes: post.upvotes - 1 },
                      { fields: ['upvotes'] })
                      .then(() => res.status(200).json({
                        message: 'Upvote removed',
                        post
                      }));
            });
        });
    }else if(isVoting ===0){
         Voting.update(
            {
                value:1
            },
            {where:{
                user_id: req.userId,
                    post_id,
            }
        })
        .then(()=>{
            Post.findOne({where:{id: post_id}}).then((post) => {
                post.update({ upvotes: post.upvotes + 1 },
                  { fields: ['upvotes'] })
                  .then((updatePost) => res.status(200).json({
                    message: 'Vote status recorded',
                    post: updatePost
                  }));
        });
    });

    }
    }
}

export default new VotingController();
