
import Sequelize, { Model } from 'sequelize';


class Voting extends Model {
  static init(sequelize) {
    super.init({
        voting: {
            type: Sequelize.INTEGER,
            allowNull: false
          },
    }, {
      sequelize,
    });
    return this;
  }
  static associate(models) {
      this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user'});
      this.belongsTo(models.Post, { foreignKey: 'post_id', as: 'post' });

  }
}

export default Voting;
