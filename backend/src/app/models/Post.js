import Sequelize, { Model } from 'sequelize';


class Post extends Model {
  static init(sequelize) {
    super.init({
      date: Sequelize.DATE,
      content: Sequelize.STRING,
      upvotes: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      downvotes: {
        allowNull: false,
        type: Sequelize.INTEGER,
        defaultValue: 0
      },


    }, {
      sequelize,
    });
    return this;
  }
  static associate(models) {
      this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user'});
      this.hasMany(models.Voting, { foreignKey: 'post_id', as: 'votings' });

  }
}

export default Post;
