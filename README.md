Faça um clone desse repositório;

BACKEND
Entre na pasta rodando cd backend;
Rode yarn para instalar as dependências;
Crie um banco de dados no postgres com o nome de posts;
Coloque as suas credenciais /config/database.js
Rode yarn sequelize db:migrate para executar as migrations;
Rode yarn dev para iniciar o servidor.

FRONTEND
Entre na pasta rodando cd nobile;
Rode yarn para instalar as dependências;
Rode yarn react-native run-ios ou yarn react-native run-android dependendo do SO.